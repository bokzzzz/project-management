<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectPart */

if(Yii::$app->user->isGuest || Yii::$app->user->identity->id != $model->project->manager0->id) {
	Yii::$app->response->redirect(['/site/login/']);
	return;
}

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Project Part',
]) . ' ' . $model->job;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Project Parts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->job, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="project-part-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
