<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectPart */

$this->title = $model->job;

?>
<div class="project-part-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= Yii::t('app', 'Project Part').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
		<?php
		if(Yii::$app->user->identity->id == $model->project->manager0->id){
            echo Html::a(Yii::t('app', 'Save As New'), ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']).'  ';            
            echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']).'  ';
            echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]);
		}
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'job',
        'man_hour',
        'percentage_done',
        [
            'attribute' => 'start_date',
            'label' => Yii::t('app', 'Start date'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
        [
            'attribute' => 'end_date',
            'label' => Yii::t('app', 'End date'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
        [
            'attribute' => 'project.name',
            'label' => Yii::t('app', 'Project'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerExternParticipant->totalCount){
    $gridColumnExternParticipant = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'participant.name',
                'label' => Yii::t('app', 'Participant')
            ],
                        'role',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerExternParticipant,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-extern-participant']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Extern Participant')),
        ],
        'export' => false,
        'columns' => $gridColumnExternParticipant
    ]);
}
?>

    </div>
	    <div class="row">
<?php
if($providerWorker->totalCount){
    $gridColumnWorker = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'user.username',
                'label' => Yii::t('app', 'Username')
            ],
                        'role',
            'time_spent:integer',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerWorker,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-worker']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Worker')),
        ],
        'export' => false,
        'columns' => $gridColumnWorker
    ]);
}
?>

    </div>
    <div class="row">
        <h4>Project<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnProject = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
            'attribute' => 'start_date',
            'label' => Yii::t('app', 'Start date'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
		[
            'attribute' => 'deadline',
            'label' => Yii::t('app', 'Deadline'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
		 [
            'attribute' => 'end_date',
            'label' => Yii::t('app', 'End date'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
        ['attribute' => 'manager0.username',
		'label' => Yii::t('app', 'Manager')
		],
        ['attribute' =>'client0.name',
		'label' => Yii::t('app', 'Client')
		],
    ];
    echo DetailView::widget([
        'model' => $model->project,
        'attributes' => $gridColumnProject    ]);
    ?>
    

</div>
