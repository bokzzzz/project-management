<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

?>
<div class="project-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->name) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
		[
            'attribute' => 'start_date',
            'label' => Yii::t('app', 'Start date'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
		[
            'attribute' => 'deadline',
            'label' => Yii::t('app', 'Deadline'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
		 [
            'attribute' => 'end_date',
            'label' => Yii::t('app', 'End date'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
        [
            'attribute' => 'manager0.username',
            'label' => Yii::t('app', 'Manager'),
        ],
        [
            'attribute' => 'client0.name',
            'label' => Yii::t('app', 'Client'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>