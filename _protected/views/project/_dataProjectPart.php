<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->projectParts,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'job',
        'man_hour',
        'percentage_done',
		[
            'attribute' => 'start_date',
            'label' => Yii::t('app', 'Start date'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
		 [
            'attribute' => 'end_date',
            'label' => Yii::t('app', 'End date'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'project-part',
			'template' => '{view}' 
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
