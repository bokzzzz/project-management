<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Finance */

$this->title = $model->project->name;

?>
<div class="finance-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= Yii::t('app', 'Finance').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
		<?php
		if(Yii::$app->user->identity->id == $model->project->manager0->id){
            echo Html::a(Yii::t('app', 'Save As New'), ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']).'  ';           
            echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']).'  ';
            echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]);
		}
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'description',
        'type',
        'amount',
		[
            'attribute' => 'payment_time',
            'label' => Yii::t('app', 'Deadline'),
			'format' => ['date', 'php:d.m.Y. h:i:s'],
			
        ],
        [
            'attribute' => 'project.name',
            'label' => Yii::t('app', 'Project'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Project<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnProject = [
        ['attribute' => 'id', 'visible' => false],
        'name',
		[
            'attribute' => 'start_date',
            'label' => Yii::t('app', 'Start date'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
		[
            'attribute' => 'deadline',
            'label' => Yii::t('app', 'Deadline'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
		 [
            'attribute' => 'end_date',
            'label' => Yii::t('app', 'End date'),
			'format' => ['date', 'php:d.m.Y.'],
			
        ],
        [
            'attribute' => 'manager0.username',
            'label' => Yii::t('app', 'Project'),
        ],
        [
            'attribute' => 'client0.name',
            'label' => Yii::t('app', 'Project'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model->project,
        'attributes' => $gridColumnProject    ]);
    ?>
</div>
