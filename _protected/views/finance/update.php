<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Finance */

if(Yii::$app->user->isGuest || Yii::$app->user->identity->id != $model->project->manager0->id) {
	Yii::$app->response->redirect(['/site/login/']);
	return;
}


$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Finance',
]) . ' ' . $model->id;

?>
<div class="finance-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
