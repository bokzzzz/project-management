<?php

namespace app\controllers;

use Yii;
use app\models\Project;
use app\models\ProjectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'pdf', 'update', 'delete', 'save-as-new', 'add-finance', 'add-project-part'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerFinance = new \yii\data\ArrayDataProvider([
            'allModels' => $model->finances,
        ]);
        $providerProjectPart = new \yii\data\ArrayDataProvider([
            'allModels' => $model->projectParts,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerFinance' => $providerFinance,
            'providerProjectPart' => $providerProjectPart,
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->request->post('_asnew') == '1') {
            $model = new Project();
        }else{
            $model = $this->findModel($id);
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }
	
	public function actionPdf($id)
    {
        $project = $this->findModel($id);
		
		require_once __DIR__ . '/../vendor/autoload.php';
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->setTitle('Project Export -' . $project->name);
		$mpdf->writeHTML('<h1 style="text-align: center;background-color:#596e79">' . $project->name.'</h1>');
		$mpdf->writeHTML('<h2 style="background-color:#c7b198;margin:0px;padding:10px">Start date: ' . $project->start_date.'</h2>');
		$mpdf->writeHTML('<h2 style="background-color:#c7b198;margin:0px;padding:10px">Project deadline: ' . $project->deadline.'</h2>');
		$mpdf->writeHTML('<h2 style="background-color:#c7b198;margin:0px;padding:10px">Project finished: ' . ($project->end_date == null ? 'Not finished' : $project->end_date).'</h2>');
		
		$mpdf->writeHTML('<br>');
		$mpdf->writeHTML('<h2 style="background-color:#596e79;margin:0px;padding:10px">Client:');
		$mpdf->writeHTML('<h4 style="background-color:#c7b198;margin:0px;padding:10px">Name: '.$project->client0->name.'</h4>');
		$mpdf->writeHTML('<h4 style="background-color:#dfd3c3;margin:0px;padding:10px">Email: '.$project->client0->email.'</h4>');
		$mpdf->writeHTML('<h2 style="background-color:#596e79;margin: 25px 0px 0px 0px ;padding:10px">Manager:');
		$mpdf->writeHTML('<h4 style="background-color:#c7b198;margin:0px;padding:10px">Name: '.$project->manager0->name.'</h4>');
		$mpdf->writeHTML('<h4 style="background-color:#dfd3c3;margin:0px;padding:10px">Surname: '.$project->manager0->surname.'</h4>');
		$mpdf->writeHTML('<h4 style="background-color:#f0ece3;margin:0px;padding:10px">Email: '.$project->manager0->email.'</h4>');
		$mpdf->writeHTML('<h2 style="background-color:#596e79;margin: 25px 0px 0px 0px ;padding:10px">Finance:</h2>');
		$brFin=1;
		foreach($project->finances as $finance){
			$mpdf->writeHTML('<h4 style="background-color:#c7b198;margin:0px;padding:10px">'.$brFin.'. </h4>');
			$mpdf->writeHTML('<h4 style="background-color:#dfd3c3;margin:0px;padding:10px">Description: '.$finance->description.'</h4>');
			$mpdf->writeHTML('<h4 style="background-color:#dfd3c3;margin:0px;padding:10px">Income/Expense: '.$finance->type.'</h4>');
			$mpdf->writeHTML('<h4 style="background-color:#dfd3c3;margin:0px;padding:10px">Amount: '.$finance->amount.'</h4>');
			$mpdf->writeHTML('<h4 style="background-color:#dfd3c3;margin:0px;padding:10px">Deadline: '.$finance->payment_time.'</h4>');
			$brFin++;
		}
		$mpdf->writeHTML('<h2 style="background-color:#596e79;margin: 25px 0px 0px 0px ;padding:10px">Project Parts:</h2>');
		$brProj=1;
		foreach($project->projectParts as $part){
			$mpdf->writeHTML('<h3 style="background-color:#c7b198;margin:0px;padding:10px">'.$brProj.'. Project Part - '.$part->job.'</h2>');
			$mpdf->writeHTML('<h5 style="background-color:#dfd3c3;margin:0px;padding:10px">Man hour: ' . $part->man_hour . '</h5>');
			$mpdf->writeHTML('<h5 style="background-color:#dfd3c3;margin:0px;padding:10px">Percentage done: ' . $part->percentage_done . '</h5>');
			$mpdf->writeHTML('<h5 style="background-color:#dfd3c3;margin:0px;padding:10px">Start date: ' . $part->start_date . '</h5>');
			$mpdf->writeHTML('<h5 style="background-color:#dfd3c3;margin:0px;padding:10px">End date: ' . $part->end_date . '</h5>');
			$mpdf->writeHTML('<h4 style="background-color:#c7b198;margin: 25px 0px 0px 0px ;padding:10px">Workers on '.$part->job.':</h4>');
			$br=1;
			foreach($part->workers as $worker){
				$mpdf->writeHTML('<h5 style="background-color:#dfd3c3;margin:0px;padding:10px">'.$br.'. </h5>');
				$mpdf->writeHTML('<h5 style="background-color:#f0ece3;margin:0px;padding:10px">User: ' . $worker->user->username . '</h5>');
				$mpdf->writeHTML('<h5 style="background-color:#f0ece3;margin:0px;padding:10px">Role:' . $worker->role . '</h5>');
				$mpdf->writeHTML('<h5 style="background-color:#f0ece3;margin:0px;padding:10px">Hours spent: ' . $worker->time_spent . '</h5>');
				$br++;
			}
			$mpdf->writeHTML('<h4 style="background-color:#c7b198;margin: 25px 0px 0px 0px;padding:10px ">Extern Participant on '.$part->job.':</h2>');
			$br=1;
			foreach($part->externParticipants as $extern){
				$mpdf->writeHTML('<h5 style="background-color:#dfd3c3;margin:0px;padding:10px">'.$br.'.</h5>');
				$mpdf->writeHTML('<h5 style="background-color:#f0ece3;margin:0px;padding:10px">Name: ' . $extern->participant->name . '</h5>');
				$mpdf->writeHTML('<h5 style="background-color:#f0ece3;margin:0px;padding:10px">Email: ' . $extern->participant->email . '</h5>');
				$mpdf->writeHTML('<h5 style="background-color:#f0ece3;margin:0px;padding:10px">Phone number: ' . $extern->participant->phone_number . '</h5>');
				$mpdf->writeHTML('<h5 style="background-color:#f0ece3;margin:0px;padding:10px">Role:' . $extern->role . '</h5>');
			}
			$brProj++;
		}
		
		//D - Force Download*/
		
		//$jsonProject = json_encode($project);
		//$mpdf->writeHTML($jsonProject);
		$mpdf->Output('Project Report -' . $project->name . '.pdf', 'D');
		
		 return $this->render('view', [
            'model' => $project,
            'providerFinance' => $providerFinance,
            'providerProjectPart' => $providerProjectPart,
        ]);
    }
    /**
    * Creates a new Project model by another data,
    * so user don't need to input all field from scratch.
    * If creation is successful, the browser will be redirected to the 'view' page.
    *
    * @param mixed $id
    * @return mixed
    */
    public function actionSaveAsNew($id) {
        $model = new Project();

        if (Yii::$app->request->post('_asnew') != '1') {
            $model = $this->findModel($id);
        }
    
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('saveAsNew', [
                'model' => $model,
            ]);
        }
    }
    
    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for Finance
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddFinance()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Finance');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formFinance', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for ProjectPart
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddProjectPart()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('ProjectPart');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formProjectPart', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
	
}
