<?php
namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Project;
use app\models\User;
use yii;

class ProjectrestController extends ActiveController
{
   public $modelClass = 'app\models\Project';
   
    public function actions() {
    $actions = parent::actions();
    unset($actions['index'],$actions['create']);
    return $actions;
}

public function actionIndex() {
	$array=\app\models\Project::find()->all();
	foreach($array as $value)
	{
		$ret[]=[
		"id" => $value->id,
		"name" => $value->name,
		"start_date" => $value->start_date,
		"deadline" => $value->deadline,
		"end_date" => $value->end_date,
		"managerName" => $value->manager0->name.' '.$value->manager0->surname,
		"clientName" => $value->client0->name
		];
	}
    return $ret;
}


public function actionCreate() {
		$message="";
		$req=Yii::$app->request;
		if($req->isPost) {
			if(!($req->post('auth_key') != null && $req->post('name') != null && $req->post('start_date') != null && $req->post('deadline') != null
			&& $req->post('end_date') != null && $req->post('manager') != null && $req->post('client') != null)) {
				$respCode=400;
				return "Bad Request";
			}
			$identity = User::findOne(['auth_key' => $req->post("auth_key")]);
			 if($identity != null)
			 {
				$project = new Project();
				$project->name = Yii::$app->request->post('name');
				$project->start_date = Yii::$app->request->post('start_date');
				$project->deadline = Yii::$app->request->post('deadline');
				$project->end_date = Yii::$app->request->post('end_date');
				$project->manager = Yii::$app->request->post('manager');
				$project->client = Yii::$app->request->post('client');
				if($project->saveAll()) {
					Yii::$app->response->statusCode = 200;
					$message="Success";
				}else{	
					Yii::$app->response->statusCode = 400;
					$message="Bad Request";
				}
			 }
			 else{
				 Yii::$app->response->statusCode = 401 ;
				 $message="Unauthorized";
			 }
		}
		else{
			Yii::$app->response->statusCode = 405  ;
			$message="Method Not Allowed";
		}
   return array('message' => $message);
}
}