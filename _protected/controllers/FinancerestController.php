<?php
namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Finance;
use app\models\User;
use yii;

class FinancerestController extends ActiveController
{
   public $modelClass = 'app\models\Finance';
   
      public function actions() {
    $actions = parent::actions();
    unset($actions['index'],$actions['create']);
    return $actions;
}

   public function actionProject(){
	   $req = Yii::$app->request;
	   $resp = Yii::$app->response;
	   $message='';
	   $respCode=200;
	    if ($req->isPost)
		{			
			if($req->post('project_id') != null ) {       
				$project_id= $req->post('project_id'); 
				$finances = Finance::find()->where(['=', 'project_id', $project_id])->asArray()->all();
		}
		}else
		{
			$name='Nije post';
			$respCode=405;
		}
		$resp->statusCode=$respCode;
		return $finances;
   }
   
   public function actionCreate() {
		$message="";
		$req=Yii::$app->request;
		if($req->isPost) {
			if(!($req->post('auth_key') != null && $req->post('description') != null && $req->post('type') != null && $req->post('amount') != null
			&& $req->post('payment_time') != null  && $req->post('project_id') != null)) {
				$respCode=400;
				return array('message' => "Bad Request body");
			}
			$identity = User::findOne(['auth_key' => $req->post("auth_key")]);
			 if($identity != null)
			 {
				$finance = new Finance();
				$finance->description = Yii::$app->request->post('description');
				$finance->type = Yii::$app->request->post('type');
				$finance->amount = Yii::$app->request->post('amount');
				$finance->payment_time = Yii::$app->request->post('payment_time');
				$finance->project_id = Yii::$app->request->post('project_id');
				if($finance->saveAll()) {
					Yii::$app->response->statusCode = 200;
					$message="Success";
				}else{	
					Yii::$app->response->statusCode = 400;
					$message="Bad Request";
				}
			 }
			 else{
				 Yii::$app->response->statusCode = 401 ;
				 $message="Unauthorized";
			 }
		}
		else{
			Yii::$app->response->statusCode = 405  ;
			$message="Method Not Allowed";
		}
   return array('message' => $message);
}

}