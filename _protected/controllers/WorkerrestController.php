<?php
namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Worker;
use app\models\User;
use yii;

class WorkerrestController extends ActiveController
{
   public $modelClass = 'app\models\Worker';
   
   
   public function actions() {
    $actions = parent::actions();
    unset($actions['index'],$actions['create']);
    return $actions;
}
   
   public function actionProject(){
	   $req = Yii::$app->request;
	   $resp = Yii::$app->response;
	   $message='';
	   $respCode=200;
	    if ($req->isPost)
		{			
			if($req->post('project_part_id') != null ) {       
				$project_part_id= $req->post('project_part_id'); 
				$workers = Worker::find()->where(['=', 'project_part_id', $project_part_id])->all();
				$ret=[];
				foreach($workers as $value)
				{
					$ret[]=[
						"name" => $value->user->name.' '.$value->user->surname,
						"role" => $value->role,
						"time_spent" => $value->time_spent
		];
	}
		}
		}else
		{
			$ret='Nije post';
			$respCode=405;
		}
		$resp->statusCode=$respCode;
		return $ret;
   }
   public function actionCreate() {
		$message="";
		$req=Yii::$app->request;
		if($req->isPost) {
			if(!($req->post('auth_key') != null && $req->post('project_part_id') != null && $req->post('role') != null && $req->post('time_spent') != null
			&& $req->post('user_id') != null )) {
				$respCode=400;
				return array('message' => "Bad Request body");
			}
			$identity = User::findOne(['auth_key' => $req->post("auth_key")]);
			 if($identity != null)
			 {
				$worker = new Worker();
				$worker->project_part_id = Yii::$app->request->post('project_part_id');
				$worker->role = Yii::$app->request->post('role');
				$worker->time_spent = Yii::$app->request->post('time_spent');
				$worker->user_id = Yii::$app->request->post('user_id');
				if($worker->saveAll()) {
					Yii::$app->response->statusCode = 200;
					$message="Success";
				}else{	
					Yii::$app->response->statusCode = 400;
					$message="Bad Request";
				}
			 }
			 else{
				 Yii::$app->response->statusCode = 401 ;
				 $message="Unauthorized";
			 }
		}
		else{
			Yii::$app->response->statusCode = 405  ;
			$message="Method Not Allowed";
		}
   return array('message' => $message);
}
}