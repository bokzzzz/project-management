<?php
namespace app\controllers;

use yii\rest\ActiveController;
use app\models\User;
use yii;

class UserrestController extends ActiveController
{
   public $modelClass = 'app\models\User';
   
   public function actions() {
    $actions = parent::actions();
    unset($actions['index']);
    return $actions;
}

public function actionIndex() {
	$array=\app\models\User::find()->orderBy('id')->asArray()->all();
	foreach($array as &$value)
		unset($value['password_hash'],$value['updated_at'],$value['created_at'],$value['account_activation_token'],$value['password_reset_token'],$value['auth_key'],$value['status']);
    return $array;
}
   
   public function actionLogin(){
	   $req = Yii::$app->request;
	   $resp = Yii::$app->response;
	   $message='';
	   $respCode=200;
	    if ($req->isPost) {			
			if($req->post('username') != null && $req->post('password') != null) {       
				$username= $req->post('username');          
				$password= $req->post('password');
				$users = User::find()->where(['=', 'username', $username])->asArray()->all();
				if(!empty($users)){
					$user=$users[0];
					if(password_verify($password, $user['password_hash']))
						$message='True';
					else $message='False';
				}
				else $message='False';
			} else {
				 $respCode=400;
				 
			}
		}else
		{
			$name='Nije post';
			$respCode=405;
		}
		$resp->statusCode=$respCode;
		if($message == 'True'){
			unset($user['updated_at'],$user['created_at'],$user['account_activation_token'],$user['password_reset_token'],$user['status'],$user['password_hash']);
			$ret=array('message' => $message,'user' => $user);
		}
		else $ret=array('message' => $message);
		return $ret;
   }
}