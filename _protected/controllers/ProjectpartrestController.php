<?php
namespace app\controllers;

use yii\rest\ActiveController;
use app\models\ProjectPart;
use app\models\User;
use yii;

class ProjectpartrestController extends ActiveController
{
   public $modelClass = 'app\models\ProjectPart';
   
    public function actions() {
    $actions = parent::actions();
    unset($actions['index'],$actions['create']);
    return $actions;
}
   
   public function actionProject(){
	   $req = Yii::$app->request;
	   $resp = Yii::$app->response;
	   $message='';
	   $respCode=200;
	    if ($req->isPost)
		{			
			if($req->post('project_id') != null ) {       
				$project_id= $req->post('project_id'); 
				$finances = ProjectPart::find()->where(['=', 'project_id', $project_id])->asArray()->all();
		}
		}else
		{
			$name='Nije post';
			$respCode=405;
		}
		$resp->statusCode=$respCode;
		return $finances;
   }
   
   public function actionCreate() {
		$message="";
		$req=Yii::$app->request;
		if($req->isPost) {
			if(!($req->post('auth_key') != null && $req->post('job') != null && $req->post('man_hour') != null && $req->post('percentage_done') != null
			&& $req->post('end_date') != null && $req->post('start_date') != null && $req->post('project_id') != null)) {
				$respCode=400;
				return "Bad Request";
			}
			$identity = User::findOne(['auth_key' => $req->post("auth_key")]);
			 if($identity != null)
			 {
				$projectPart = new ProjectPart();
				$projectPart->job = Yii::$app->request->post('job');
				$projectPart->percentage_done = Yii::$app->request->post('percentage_done');
				$projectPart->man_hour = Yii::$app->request->post('man_hour');
				$projectPart->end_date = Yii::$app->request->post('end_date');
				$projectPart->start_date = Yii::$app->request->post('start_date');
				$projectPart->project_id = Yii::$app->request->post('project_id');
				if($projectPart->saveAll()) {
					Yii::$app->response->statusCode = 200;
					$message="Success";
				}else{	
					Yii::$app->response->statusCode = 400;
					$message="Bad Request";
				}
			 }
			 else{
				 Yii::$app->response->statusCode = 401 ;
				 $message="Unauthorized";
			 }
		}
		else{
			Yii::$app->response->statusCode = 405  ;
			$message="Method Not Allowed";
		}
   return array('message' => $message);
}

}