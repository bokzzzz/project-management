<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "participant".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone_number
 *
 * @property \app\models\ExternParticipant[] $externParticipants
 * @property \app\models\ProjectPart[] $projectParts
 * @property \app\models\Project[] $projects
 */
class Participant extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'externParticipants',
            'projectParts',
            'projects'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone_number'], 'required'],
            [['name', 'email', 'phone_number'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participant';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'phone_number' => Yii::t('app', 'Phone Number'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExternParticipants()
    {
        return $this->hasMany(\app\models\ExternParticipant::className(), ['participant_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectParts()
    {
        return $this->hasMany(\app\models\ProjectPart::className(), ['id' => 'project_part_id'])->viaTable('extern_participant', ['participant_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(\app\models\Project::className(), ['client' => 'id']);
    }
    

    /**
     * @inheritdoc
     * @return \app\models\ParticipantQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ParticipantQuery(get_called_class());
    }
}
