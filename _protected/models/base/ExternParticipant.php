<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "extern_participant".
 *
 * @property integer $participant_id
 * @property integer $project_part_id
 * @property string $role
 *
 * @property \app\models\Participant $participant
 * @property \app\models\ProjectPart $projectPart
 */
class ExternParticipant extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'participant',
            'projectPart'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['participant_id', 'project_part_id', 'role'], 'required'],
            [['participant_id', 'project_part_id'], 'integer'],
            [['role'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'extern_participant';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'participant_id' => Yii::t('app', 'Participant ID'),
            'project_part_id' => Yii::t('app', 'Project Part ID'),
            'role' => Yii::t('app', 'Role'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipant()
    {
        return $this->hasOne(\app\models\Participant::className(), ['id' => 'participant_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectPart()
    {
        return $this->hasOne(\app\models\ProjectPart::className(), ['id' => 'project_part_id']);
    }
    

    /**
     * @inheritdoc
     * @return \app\models\ExternParticipantQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ExternParticipantQuery(get_called_class());
    }
}
