<?php

namespace app\models;

use Yii;
use \app\models\base\Worker as BaseWorker;

/**
 * This is the model class for table "worker".
 */
class Worker extends BaseWorker
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'project_part_id', 'time_spent'], 'required'],
            [['user_id', 'project_part_id', 'time_spent'], 'integer'],
            [['role'], 'string', 'max' => 255]
        ]);
    }
	
}
