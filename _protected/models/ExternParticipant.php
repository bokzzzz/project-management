<?php

namespace app\models;

use Yii;
use \app\models\base\ExternParticipant as BaseExternParticipant;

/**
 * This is the model class for table "extern_participant".
 */
class ExternParticipant extends BaseExternParticipant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['participant_id', 'project_part_id', 'role'], 'required'],
            [['participant_id', 'project_part_id'], 'integer'],
            [['role'], 'string', 'max' => 45]
        ]);
    }
	
}
