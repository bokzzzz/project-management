<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProjectPart;

/**
 * app\models\ProjectPartSearch represents the model behind the search form about `app\models\ProjectPart`.
 */
 class ProjectPartSearch extends ProjectPart
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'man_hour', 'project_id'], 'integer'],
            [['job', 'percentage_done', 'start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProjectPart::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'man_hour' => $this->man_hour,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'project_id' => $this->project_id,
        ]);

        $query->andFilterWhere(['like', 'job', $this->job])
            ->andFilterWhere(['like', 'percentage_done', $this->percentage_done]);

        return $dataProvider;
    }
}
