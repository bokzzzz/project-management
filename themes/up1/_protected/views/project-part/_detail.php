<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectPart */

?>
<div class="project-part-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->job) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'job',
        'man_hour',
        'percentage_done',
        'start_date',
        'end_date',
        [
            'attribute' => 'project.name',
            'label' => Yii::t('app', 'Project'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>