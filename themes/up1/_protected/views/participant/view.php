<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Participant */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Participants'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="participant-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= Yii::t('app', 'Participant').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a(Yii::t('app', 'Save As New'), ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'email:email',
        'phone_number',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerExternParticipant->totalCount){
    $gridColumnExternParticipant = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'projectPart.job',
                'label' => Yii::t('app', 'Project Part')
            ],
            'role',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerExternParticipant,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-extern-participant']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Extern Participant')),
        ],
        'export' => false,
        'columns' => $gridColumnExternParticipant
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerProject->totalCount){
    $gridColumnProject = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
            'start_date',
            'deadline',
            'end_date',
            [
                'attribute' => 'manager0.username',
                'label' => Yii::t('app', 'Manager')
            ],
                ];
    echo Gridview::widget([
        'dataProvider' => $providerProject,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-project']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Project')),
        ],
        'export' => false,
        'columns' => $gridColumnProject
    ]);
}
?>

    </div>
</div>
