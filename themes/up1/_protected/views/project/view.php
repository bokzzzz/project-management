<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= Yii::t('app', 'Project').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a(Yii::t('app', 'Save As New'), ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'start_date',
        'deadline',
        'end_date',
        [
            'attribute' => 'manager0.username',
            'label' => Yii::t('app', 'Manager'),
        ],
        [
            'attribute' => 'client0.name',
            'label' => Yii::t('app', 'Client'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerFinance->totalCount){
    $gridColumnFinance = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'description',
            'type',
            'amount',
            'payment_time',
                ];
    echo Gridview::widget([
        'dataProvider' => $providerFinance,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-finance']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Finance')),
        ],
        'export' => false,
        'columns' => $gridColumnFinance
    ]);
}
?>

    </div>
    <div class="row">
        <h4>Participant<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnParticipant = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'email',
        'phone_number',
    ];
    echo DetailView::widget([
        'model' => $model->client0,
        'attributes' => $gridColumnParticipant    ]);
    ?>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        'email',
        'password_hash',
        'status',
        'auth_key',
        'password_reset_token',
        'account_activation_token',
    ];
    echo DetailView::widget([
        'model' => $model->manager0,
        'attributes' => $gridColumnUser    ]);
    ?>
    
    <div class="row">
<?php
if($providerProjectPart->totalCount){
    $gridColumnProjectPart = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'job',
            'man_hour',
            'percentage_done',
            'start_date',
            'end_date',
                ];
    echo Gridview::widget([
        'dataProvider' => $providerProjectPart,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-project-part']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('app', 'Project Part')),
        ],
        'export' => false,
        'columns' => $gridColumnProjectPart
    ]);
}
?>

    </div>
</div>
