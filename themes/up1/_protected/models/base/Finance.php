<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "finance".
 *
 * @property integer $id
 * @property string $description
 * @property string $type
 * @property string $amount
 * @property string $payment_time
 * @property integer $project_id
 *
 * @property \app\models\Project $project
 */
class Finance extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'project'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'type', 'amount', 'payment_time', 'project_id'], 'required'],
            [['amount'], 'number'],
            [['payment_time'], 'safe'],
            [['project_id'], 'integer'],
            [['description'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'finance';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'type' => Yii::t('app', 'Income/Expense'),
            'amount' => Yii::t('app', 'Amount'),
            'payment_time' => Yii::t('app', 'Payment time'),
            'project_id' => Yii::t('app', 'Project ID'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(\app\models\Project::className(), ['id' => 'project_id']);
    }
    

    /**
     * @inheritdoc
     * @return \app\models\FinanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\FinanceQuery(get_called_class());
    }
}
