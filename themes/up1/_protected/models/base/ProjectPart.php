<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "project_part".
 *
 * @property integer $id
 * @property string $job
 * @property integer $man_hour
 * @property integer $percentage_done
 * @property string $start_date
 * @property string $end_date
 * @property integer $project_id
 *
 * @property \app\models\ExternParticipant[] $externParticipants
 * @property \app\models\Participant[] $participants
 * @property \app\models\Project $project
 * @property \app\models\Worker[] $workers
 * @property \app\models\User[] $users
 */
class ProjectPart extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'externParticipants',
            'participants',
            'project',
            'workers',
            'users'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['job', 'man_hour', 'start_date', 'end_date', 'project_id'], 'required'],
            [['man_hour', 'project_id'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['job'], 'string', 'max' => 255],
            [['percentage_done'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_part';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'job' => Yii::t('app', 'Job description'),
            'man_hour' => Yii::t('app', 'Man hour'),
            'percentage_done' => Yii::t('app', 'Percentage done'),
            'start_date' => Yii::t('app', 'Start date'),
            'end_date' => Yii::t('app', 'End date'),
            'project_id' => Yii::t('app', 'Project'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExternParticipants()
    {
        return $this->hasMany(\app\models\ExternParticipant::className(), ['project_part_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParticipants()
    {
        return $this->hasMany(\app\models\Participant::className(), ['id' => 'participant_id'])->viaTable('extern_participant', ['project_part_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(\app\models\Project::className(), ['id' => 'project_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(\app\models\Worker::className(), ['project_part_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(\app\models\User::className(), ['id' => 'user_id'])->viaTable('worker', ['project_part_id' => 'id']);
    }
    

    /**
     * @inheritdoc
     * @return \app\models\ProjectPartQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ProjectPartQuery(get_called_class());
    }
}
